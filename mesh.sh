#!/bin/bash

echo Init
for key in `seq 1 100` 
do
    url="http://localhost:4040/create?key=key$key&value=$key&ttl=60"
    curl $url
done

echo Read
for key in `seq 1 100` 
do
    url="http://localhost:4040/read?key=key$key"
    curl $url
done

echo Change mul 2
for key in `seq 1 100` 
do
    let val=key*2
    url="http://localhost:4040/update?key=key$key&value=$val&ttl=60"
    curl $url
done

echo Read again
for key in `seq 1 100` 
do
    url="http://localhost:4040/read?key=key$key"
    curl $url
done

echo Delete
for key in `seq 1 100` 
do
    url="http://localhost:4040/delete?key=key$key"
    curl $url
done

echo Read after delete
for key in `seq 1 100` 
do
    url="http://localhost:4040/read?key=key$key"
    curl $url
done

echo Create with ttl=1
for key in `seq 1 100` 
do
    url="http://localhost:4040/create?key=key$key&value=$key&ttl=1"
    curl $url
done

sleep 2
echo Read expire ttl
for key in `seq 1 100` 
do
    url="http://localhost:4040/read?key=key$key"
    curl $url
done
