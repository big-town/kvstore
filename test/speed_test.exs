defmodule SpeedTest do
  use ExUnit.Case, async: false
  use Plug.Test

  @tag :skip
  @doc "Тестирование скорости DETS"
  test "test speed dets" do
    :dets.open_file(:disk_storage, [type: :set,  auto_save: 100 ])
    for f<-1..100000 do
      :dets.insert_new(:disk_storage, {"key"<>Integer.to_string(f),Integer.to_string(f+2)} )
    end
    """
    Тест показал при 100 000 итераций при первом запуске затрачено 9.6сек.
    При последующем 1.2сек.
    При перезаписи ключей с другим значением 1.4сек.
    """
  end
  @tag :skip
  @doc "Тестирование скорости ETS"
  test "test speed ets" do
    :ets.new(:user_lookup, [:set, :protected, :named_table])
    for f<-1..100000 do
      :ets.insert_new(:user_lookup, {"key"<>Integer.to_string(f),Integer.to_string(f+2)} )
    end
    assert :ets.tab2file(:user_lookup, 'etstofile') == :ok
    """
    Тест показал при 100 000 итераций при любом запуске затрачено 0.2сек.
    """
  end
end
