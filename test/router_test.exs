defmodule RouterTest do
  use ExUnit.Case, async: true
  use Plug.Test
  alias KvRouter, as: Router
  doctest KvRouter

  #@tag :skip
  @doc """
  Проверяем существующий роутинг на все методы
  """
  test "test malformed key" do
    conn =
      conn(:get, "http://localhost:4040/create?value=val1&ttl=6")
      |> Router.call([])
    assert conn.status==200
  end

  @tag :skip
  test "test exists method" do
    conn =
      conn(:get, "http://localhost:4040/create?key=key1&value=val1&ttl=6")
      |> Router.call([])
    assert conn.status==200
    conn =
      conn(:get, "http://localhost:4040/read?key=key1")
      |>  Router.call([])
    assert conn.status==200
    conn =
      conn(:get, "http://localhost:4040/update?key=key1&value=val1&ttl=8")
      |>  Router.call([])
    assert conn.status==200
    conn =
      conn(:get, "http://localhost:4040/read?key=key1")
      |> Router.call([])
    assert conn.status==200
  end
  @tag :skip
  @doc """
  Проверяем несуществующий роутинг на все методы
  """
  test "test not exists method" do
    conn =
      conn(:get, "http://localhost:4040/saddfsdgdsfgsd")
      |> Router.call([])
    assert conn.status==404
    conn =
      conn(:get, "http://localhost:4040/saddfsdgdsfgsd")
      |> Router.call([])
    assert conn.status==404
    conn =
      conn(:get, "http://localhost:4040/saddfsdgdsfgsd")
      |> Router.call([])
    assert conn.status==404
    conn =
      conn(:get, "http://localhost:4040/saddfsdgdsfgsd")
      |> Router.call([])
      assert conn.status==404
  end
end
