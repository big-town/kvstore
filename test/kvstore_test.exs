defmodule KvStoreTest do
  use ExUnit.Case, async: true
  doctest KvStore

  setup_all do
    :dets.view(:name1)
    Storage.open(:name1)
    :ok
  end

  @tag :skip
  test "test expired" do
    IO.puts "Создаем элемент"
    Storage.delete(:name1, 1234)
    assert Storage.create(:name1, 1234, 000000, 3)
    #Process.sleep(4000)
    IO.puts "Обновляем"
    assert Storage.update(:name1, 1234, 000000, 3)
    IO.puts "читаем"
    assert Storage.read(:name1, 1234)
    IO.puts "Ждем с текущего момента 4с"
    Process.sleep(4000)
    IO.puts "Запись устареет вернет false"
    refute Storage.read(:name1, 1234)
  end
  #"Тестирование open создан файл с root:root 400"
  #test "test open" do
  #  assert  Storage.open(:name) == {:error,:not_opened}
  #end
  # "Тестирование storage"

end
