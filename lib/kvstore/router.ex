defmodule KvRouter do
  use Plug.Router
  use Plug.Debugger
  require Logger
  alias Plug.Conn.Query, as: Qw

  plug(Plug.Logger, log: :debug)
  plug(:match)
  plug(:dispatch)
  @dets_name :kvstore

  @moduledoc """
  Реализуем CRUD API
  POST, GET, PUT, DELETE
  create, read, update, delete
  """

  get "/create" do
    qs=conn.query_string
    key=Qw.decode(qs)["key"]
    value=Qw.decode(qs)["value"]
    ttl=Qw.decode(qs)["ttl"]

    if Storage.create(@dets_name,key,value,ttl) do
      send_resp(conn, 200, "OK")
    else
      send_resp(conn, 200, "ERROR")
    end
  end

  get "/read" do
    qs=conn.query_string
    key=Qw.decode(qs)["key"]
    object=Storage.read(@dets_name,key)

    if object do
      send_resp(conn, 200, object )
    else
      send_resp(conn, 200, "ERROR")
    end
  end

 get "/update" do
    qs=conn.query_string
    key=Qw.decode(qs)["key"]
    value=Qw.decode(qs)["value"]
    ttl=String.to_integer(Qw.decode(qs)["ttl"])

    if Storage.update(@dets_name,key,value,ttl) do
      send_resp(conn, 200, "OK")
    else
      send_resp(conn, 200, "ERROR")
    end
  end

  get "/delete" do
    qs=conn.query_string
    key=Qw.decode(qs)["key"]

    if Storage.delete(@dets_name,key) do
      send_resp(conn, 200, "OK")
    else
      send_resp(conn, 200, "ERROR")
    end
  end

  #Выдаем корневую страницу
  get "/" do
    send_file(conn, 200, "html/index.html")
  end

  #Сюда попадаем если нет соответствия API
  match _ do
    send_resp(conn, 404, "Entry API not found!")
  end

end
