defmodule Storage do
  require Logger

  @moduledoc """
  Функции для работы с KV хранилищем.
  Не грузим фрондендщиков и возвращаем только false или true, то есть получилось или нет.
  При чтении обновлении и вставке учитываем TTL
  """

  @doc """
  Создает KV хранилище
  """

  @spec open(atom()) :: {:ok, atom()} | {:error ,:not_opened}
  def open(name) do
    case :dets.open_file( name, [type: :set]) do
    {:ok, name} -> {:ok, name}
    {:error, reason} ->
      Logger.error "#{__MODULE__}.Cannot_open_DETS: #{inspect reason}"
      {:error, :not_opened}
    end
  end
  #Функция вытаскивает из бинарной даты число, если данные полностью невалидные то присваивает значение по умолчанию
  @spec int_ttl(binary()) :: integer
  defp int_ttl(s) do
    case Integer.parse(to_string(s)) do
      {ttl,_} -> ttl
      :error -> 10 #default ttl
    end
  end

  @doc """
  Создает KV элимент
  Если элемент новый или ttl уже истек возвращаем true иначе false
  """
  @spec create(atom(),binary(),binary(),binary()) :: true | false
  def create(name,key,value,ttl) do
    try do
      expire = :os.system_time(:seconds) + int_ttl(ttl)
      #IO.inspect binding()
      read(name,key)#Если элемент существует но TTL истек то функция read его удалит и будет возвращен true
      case :dets.insert_new(name,{key,value,expire}) do
        true -> true
        _ -> false
      end
    rescue
      err ->
        Logger.error(Exception.format(:error, err, __STACKTRACE__))
      false
    end
  end

  @doc """
  Обновляем KV элемент при обновлении учитываем истекший TTL
  Если элемент существует и ttl не истек, то обновляем и возвращаем true иначе false
  """
  @spec update(atom(),binary(),binary(),binary()) :: true | false
  def update(name,key,value,ttl) do
    expired = :os.system_time(:seconds) + int_ttl(ttl)
    if read(name,key) do
      #Предусматриваем ошибку чтения из несуществующей таблицы потому обрабатываем исключение
      #в этом случае не возвращается {:error, reason}
      try do
        case :dets.insert(name, {key,value,expired}) do
          :ok -> true
          _ -> false
        end
      rescue
        err ->
          Logger.error(Exception.format(:error, err, __STACKTRACE__))
        false
      end
    else
      false
    end
  end

  @doc """
  Читаем KV элемент при этом учитываем истекший TTL
  Если элемент существует и ttl не истек возвращаем true иначе false
  """
  @spec read(atom(), binary()) :: false | String
  def read(name,key) do
    #Предусматриваем ошибку чтения из несуществующей таблицы потому обрабатываем исключение
    #в этом случае не возвращается {:error, reason}
    try do
      res=:dets.lookup(name, key)
      #IO.inspect binding()
      case res do
        [] -> false
        [object] ->
          #Если время меньше системного удаляем иначе возвращаем сам объект
          if elem(object,2) < :os.system_time(:seconds) do
            :dets.delete(name, key)
            false
          else
            "#{inspect object}"
          end
      end
    rescue
      err ->
        Logger.error(Exception.format(:error, err, __STACKTRACE__))
      false
    end
  end

  @doc """
  Удаляем KV элемент
  """
  @spec delete(atom(),binary()) :: false | true
  def delete(name,key) do
    #Предусматриваем ошибку чтения из несуществующей таблицы потому обрабатываем исключение
    #в этом случае не возвращается {:error, reason}
    try do
      case :dets.delete(name, key) do
        :ok -> true
        _ -> false
      end
    rescue
      err ->
        Logger.error(Exception.format(:error, err, __STACKTRACE__))
      false
    end
  end
end
