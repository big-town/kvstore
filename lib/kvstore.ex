defmodule KvStore do
  use Application
  require Logger
  @dets_name :kvstore
  @moduledoc """
  Самая простая реализация key=>value хранилища
  """

  @doc """
  Реализуем start для автозапуска приложения
  Точка входа
  """
  def start(_type, _args) do
    children = [
      {Plug.Cowboy, scheme: :http, plug: KvRouter, options: [port: 4040]}
    ]
    Logger.info("Started application")
    state=Supervisor.start_link(children, strategy: :one_for_one) |> IO.inspect

    #Нет смысла запускать сервис если DETS не может быть открыт поэтому не перехватываем исключение
    case :dets.open_file(@dets_name , [type: :set, access: :read_write ]) do
      {:ok,_} ->  Logger.info("DETS opened")
                  state
      _ -> Logger.error("DETS did not open!")
    end
  end
end
