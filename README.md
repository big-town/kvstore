# Kvstore
Реализация самого простокго хранилища
Сервер слушает запросы на http://localhost:4040

При переходе откроется корневая страница где реализованы
CRUD API через методы POST, GET, PUT, DELETE

API имеет соответствующие ссылки
  /create, /read, /update, /delete

Example:

    http://localhost:4040/create?key=key1&value=123&ttl=60
    
    http://localhost:4040/read?key=key1
    
    http://localhost:4040/update?key=key1&value=123&ttl=60
    
    http://localhost:4040/delete?key=key1
    

Есть скрипт bash mesh.sh для автотестирования